package it.uniroma3.siw.progetto;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.uniroma3.siw.progetto.model.Comment;
import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.model.Task;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.CommentRepository;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;
import it.uniroma3.siw.progetto.repository.ProjectRepository;
import it.uniroma3.siw.progetto.repository.TagRepository;
import it.uniroma3.siw.progetto.repository.TaskRepository;
import it.uniroma3.siw.progetto.repository.UserRepository;
import it.uniroma3.siw.progetto.service.ProjectService;
import it.uniroma3.siw.progetto.service.TaskService;


@SpringBootTest
@RunWith(SpringRunner.class)
class TaskTest {

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private CredentialsRepository credentialsRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private TaskService taskService;
	

	@Before
	public void deleteAll() {
		System.out.println("Deleting all data...");
		this.commentRepository.deleteAll();
		this.credentialsRepository.deleteAll();
		this.projectRepository.deleteAll();
		this.tagRepository.deleteAll();
		this.taskRepository.deleteAll();
		this.userRepository.deleteAll();
		System.out.println("Deleting all data...");
	}
	
	@AfterEach
	public void deleteAfterEach() {
		System.out.println("Deleting all data...");
		this.commentRepository.deleteAll();
		this.credentialsRepository.deleteAll();
		this.projectRepository.deleteAll();
		this.tagRepository.deleteAll();
		this.taskRepository.deleteAll();
		this.userRepository.deleteAll();
		System.out.println("Deleting all data...");
	}

	/*
	 * Creo 1 progetto con 2 task
	 * salvo tutto nel db
	 * faccio l'update di un task
	 * faccio il delete di un task
	 */
	
	@Test
	void testCreateUpdateDeleteTask() {
		Project project1 = new Project("progetto");
		Task task1 = new Task("task1", "descrizione1");
		Task task2 = new Task("task2", "descrizione2");
		project1.addTask(task1);
		project1.addTask(task2);
		task1 = taskRepository.save(task1);
		task2 = taskRepository.save(task2);
		project1 = projectRepository.save(project1);
		/* equivalente di fare save(task1) e poi addTaskToProject(project1, task1) */
		List<Task> tasksList = project1.getTasks();
		assertTrue(tasksList.contains(task1));
		assertTrue(tasksList.contains(task2));
		assertEquals(task1.getName(), "task1");
		assertEquals(task2.getName(), "task2");
		assertEquals(task1.getDescription(), "descrizione1");
		assertEquals(task2.getDescription(), "descrizione2");
		
		Task task1Update = new Task ("task1update", "descrizione1update");
		task1Update.setId(task1.getId());	
		task1Update.setCreationTimestamp(task1.getCreationTimestamp());
		task1Update = taskRepository.save(task1Update);
		Project project1update1 = projectService.getProject(project1.getId());
		assertEquals(task1Update.getName(), "task1update");
		assertEquals(task1Update.getDescription(), "descrizione1update");
		List<Task> tasksUpdatedList = project1update1.getTasks();
		assertTrue(tasksUpdatedList.contains(task1Update));
		
		taskService.deleteTask(task1Update);
		Project project1update2 = projectService.getProject(project1.getId());
		List<Task> tasksDeletedList = project1update2.getTasks();
		assertFalse(tasksDeletedList.contains(task1Update));
	}
	
	@Test
	void testAssignTaskToUser() {
		Task task4User = new Task("task", "descrizione");
		User user = new User("nome", "cognome");
		task4User.setAssignedTo(user);
		user = userRepository.save(user);
		task4User = taskRepository.save(task4User);
		assertEquals(task4User.getAssignedTo(), user);
	}
	
	@Test
	void testAddTagToTask() {
		Tag tag = new Tag("importante", "RED");
		Task task = new Task("nome", "descrizione");
		task.addTag(tag);
		tag = tagRepository.save(tag);
		task = taskRepository.save(task);
		List<Tag> tagList = task.getTags();
		assertTrue(tagList.contains(tag));
	}
	
	
	@Test
	void testAddCommentToTask() {
		User user = new User("Mario", "Rossi");
		Comment comment = new Comment("Commento", user);
		Task task = new Task("nome", "descrizione");
		task.addComment(comment);
		user = userRepository.save(user);
		comment = commentRepository.save(comment);
		task = taskRepository.save(task);
		List<Comment> commentList = task.getComments();
		assertTrue(commentList.contains(comment));
	}
	
	@Test
	void testRemoveUserFromTask() {
		User user = new User("Mario", "Rossi");
		Task task = new Task("nome", "descrizione");
		task.setAssignedTo(user);
		user = userRepository.save(user);
		task = taskRepository.save(task);
		assertEquals(task.getAssignedTo(), user);
		task.setAssignedTo(null);
		task = taskRepository.save(task);
		assertEquals(task.getAssignedTo(), null);
	}


}