package it.uniroma3.siw.progetto;

import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.CommentRepository;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;
import it.uniroma3.siw.progetto.repository.ProjectRepository;
import it.uniroma3.siw.progetto.repository.TagRepository;
import it.uniroma3.siw.progetto.repository.TaskRepository;
import it.uniroma3.siw.progetto.repository.UserRepository;
import it.uniroma3.siw.progetto.service.ProjectService;
import it.uniroma3.siw.progetto.service.TagService;
import it.uniroma3.siw.progetto.service.UserService;


@SpringBootTest
@RunWith(SpringRunner.class)
class ProjectTest {

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private CredentialsRepository credentialsRepository;

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private UserService userService;
	

	@Autowired
	private TagService tagService;



	@BeforeEach
	public void deleteAll() {
		System.out.println("Deleting all data...");
		this.commentRepository.deleteAll();
		this.credentialsRepository.deleteAll();
		this.projectRepository.deleteAll();
		this.tagRepository.deleteAll();
		this.taskRepository.deleteAll();
		this.userRepository.deleteAll();
		System.out.println("Deleting all data...");
	}

	
	
	@Test
	public void testSaveProject() {

		User user1 = new User("nome1","cognome1");

		Project project1 = new Project("test1");
		project1.setOwner(user1);
		
		user1= userService.saveUser(user1);
		project1 = projectService.saveProject(project1);
		assertEquals(project1.getOwner(), user1);
		assertEquals(project1.getName(), "test1");
		
		List<Project> projects = projectRepository.findByOwner(user1);
		assertEquals(projects.size(), 1);
		assertEquals(projects.get(0), project1);
		
	}
	
	
	
	@Test
	public void testUpdateProject() {

		Project project1 = new Project("test1");		
		
		project1 = projectService.saveProject(project1);
		assertEquals(project1.getName(), "test1");

		Project project1Update = new Project("testUpdate");
		project1Update.setId(project1.getId());
		project1Update.setCreationDate(project1.getCreationDate());
		project1Update = projectService.saveProject(project1Update);
		assertEquals(project1Update.getName(), "testUpdate");	
		
	}
	
	
	@Test
	public void testSharedProject() {
		Project project1 = new Project("test1");
		User user1 = new User("nome1","cognome1");
		User user2 = new User("nome2","cognome2");
		
		project1.setOwner(user1);
		
		user1= userService.saveUser(user1);
		user2= userService.saveUser(user2);
		project1 = projectService.saveProject(project1);

		
		project1.addMember(user2);
		project1= projectService.saveProject(project1);
		
		project1 = projectService.shareProjectWithUser(project1, user2);
		

		List<Project> projectsVisibleByUser2 = projectRepository.findByMembers(user2);
		assertEquals(projectsVisibleByUser2.size(), 1);
		assertEquals(projectsVisibleByUser2.get(0), project1);

		List<User> project1Members = userRepository.findByVisibleProjects(project1);
		assertEquals(project1Members.size(), 1);
		assertEquals(project1Members.get(0), user2);

	}
	
	
	
	@Test
	public void testDeleteProject() {

		User user1 = new User("nome1","cognome1");

		Project project1 = new Project("test1");
		project1.setOwner(user1);
		
		user1= userService.saveUser(user1);
		project1 = projectService.saveProject(project1);
		assertEquals(project1.getOwner(), user1);
		assertEquals(project1.getName(), "test1");
		
		List<Project> projects = projectRepository.findByOwner(user1);
		assertEquals(projects.size(), 1);
		assertEquals(projects.get(0), project1);
		
		projectService.deleteProject(project1);
		List<Project> projectOwned = projectRepository.findByOwner(user1);
		assertEquals(projectOwned.size(), 0);
		
	}
	
	
	@Test
	public void testAddTag() {
		Project project1 = new Project("test1");
		Tag tag1 = new Tag("aaa", "fff");
		
		project1.addTag(tag1);
		
		tag1 = tagService.saveTag(tag1);
		project1 = projectService.saveProject(project1);
	
		//aggiunta standard
		List<Tag> projectsTags = project1.getTags();
		assertEquals(projectsTags.size(), 1);
		
		//aggiornamento
		Tag tag2 = new Tag("bbb","111");
		tag2 = tagService.saveTag(tag2);
		project1 = projectService.addTagToProject(project1, tag2);
		projectsTags = project1.getTags();
		assertEquals(projectsTags.size(), 2);
		assertEquals(projectsTags.get(1), tag2);
		
		//aggiunta di un doppione
		Tag tag2copy= new Tag("bbb","111");
		tag2copy.setId(tag2.getId());
		tag2copy = tagService.saveTag(tag2copy);
		project1 = projectService.addTagToProject(project1, tag2copy);
		projectsTags = project1.getTags();
		assertEquals(projectsTags.size(), 2);
		assertEquals(projectsTags.get(1), tag2);
	}
	

}