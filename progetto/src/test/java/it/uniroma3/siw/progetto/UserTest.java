package it.uniroma3.siw.progetto;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;
import it.uniroma3.siw.progetto.repository.UserRepository;
import it.uniroma3.siw.progetto.service.CredentialsService;
import it.uniroma3.siw.progetto.service.UserService;


@SpringBootTest
@RunWith(SpringRunner.class)
class UserTest {

	@Autowired
	private CredentialsRepository credentialsRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CredentialsService credentialsService;

	@Autowired
	private UserService userService;

	@Before
	public void deleteAll() {
		System.out.println("Deleting all data...");
		this.credentialsRepository.deleteAll();
		this.userRepository.deleteAll();
		System.out.println("Deleting all data...");
	}

	@AfterEach
	public void deleteAfterEach() {
		System.out.println("Deleting all data...");
		this.credentialsRepository.deleteAll();
		this.userRepository.deleteAll();
		System.out.println("Deleting all data...");
	}

	@Test
	void testCreateAndUpdateUserAndCredentials() {

		// creo 2 utenti e le credenziali e li inserisco
		// le prime credenziali hanno id 1 utente id 2
		// le seconde credenziali hanno id 3 utente id 4
		// GenerationType.AUTO genera id unici a livello db
		// https://www.objectdb.com/java/jpa/entity/generated
		
		Credentials credentials1 = new Credentials("mariorossi", "password");
		User user1 = new User ("mario", "rossi");
		credentials1.setUser(user1);
		credentials1 = credentialsService.saveCredentials(credentials1);
		user1 = userService.saveUser(user1);
		assertEquals(user1.getId().longValue(), 2L);
		assertEquals(user1.getFirstName(), "mario");
		assertEquals(user1.getLastName(), "rossi");
		assertEquals(credentials1.getUser(), user1);
		assertEquals(credentials1.getUserName(), "mariorossi");

		Credentials credentials2 = new Credentials("lucaverdi", "password");
		User user2 = new User ("luca", "verdi");
		credentials2.setUser(user2);
		credentials2 = credentialsService.saveCredentials(credentials2);
		user2 = userService.saveUser(user2);
		assertEquals(user2.getId().longValue(), 4L);
		assertEquals(user2.getFirstName(), "luca");
		assertEquals(user2.getLastName(), "verdi");
		assertEquals(credentials2.getUser(), user2);
		assertEquals(credentials2.getUserName(), "lucaverdi");
	
		//update di credenziali e utente 1
		
		User user1Update = new User ("maria", "rossi");
		Credentials credentials1Update = new Credentials("mariarossi", "password");
		credentials1Update.setId(credentials1.getId());
		user1Update.setId(user1.getId());
		credentials1Update = credentialsService.saveCredentials(credentials1Update);
		user1Update = userService.saveUser(user1Update);
		assertEquals(credentials1Update.getId().longValue(), 1L);
		assertEquals(credentials1Update.getUserName(), "mariarossi");
		assertEquals(user1Update.getId().longValue(), 2L);
		assertEquals(user1Update.getFirstName(), "maria");
	}
}