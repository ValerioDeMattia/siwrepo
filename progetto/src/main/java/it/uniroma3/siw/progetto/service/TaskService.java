package it.uniroma3.siw.progetto.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Comment;
import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.model.Task;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.TaskRepository;

@Service
public class TaskService {
	
	@Autowired
    protected TaskRepository taskRepository;

    /**
     * Recupera un' attività dal DB in base al suo ID
     * @param id l'id dell'attività che vogliamo recuperare dal DB
     * @return l' attività recuperata, oppure null se non c'è corrispondenza dell'ID nel DB
     */
    @Transactional
    public Task getTask(long id) {
        Optional<Task> result = this.taskRepository.findById(id);
        return result.orElse(null);
    }

    /**
     * Effettua la save dell'attività sul BD
     * @param task l'attività da salvare sul DB
     * @return l'attività salvata sul DB
     */
    @Transactional
    public Task saveTask(Task task) {
        return this.taskRepository.save(task);
    }

 

    /**
     * Effettua la cancellazione di un'attività dal DB
     * @param task l'attività eliminata dal db
     */
    @Transactional
    public void deleteTask(Task task) {
        this.taskRepository.delete(task);
    }

    /**
     * Aggiungi un commento ad un task
     * @param comment, il commento
     * @param task, il task a cui aggiungo il commento
     * @return il task aggiornato
     */
    @Transactional
    public Task postCommentOnTask(Comment comment, Task task) {
        task.addComment(comment);
        return this.taskRepository.save(task);
    }
    
	/**
	 * Aggiunge un nuovo tag al progetto e lo salva nel db
	 * @param project, il progetto 
	 * @param tag, il tag da aggiungere al progetto
	 * @return il progetto con il nuovo tag
	 */
	@Transactional
	public Task assignTaskToMember(Task task, User member) {
		task.setAssignedTo(member);
		return this.taskRepository.save(task);
	}


	/**
	 * Elimina un tag dal db e lo rimuove dalle task in cui compare
	 * @param project tag, il tag da eliminare e il progetto in cui compare
	 * @param project, il progetto da cui rimuovere il tag
	 * @return il progetto condiviso
	 */
	@Transactional
	public Task deassignMemberFromTask(Task task) {
		task.setAssignedTo(null);
		return  this.taskRepository.save(task);
	}
	
	    /**
     * Aggiunge un tag esistente(già è stata fatta la save in precedenza) alla task selezionata
     * @param task tag, la tag da aggiungere e la task alla quale aggiungere la tag
     * @return task, la task aggiornata
     */
    @Transactional
    public Task addTagToTask(Task task, Tag tag) {
    	task.addTag(tag);
    	return this.taskRepository.save(task);
    }
    
    /**
     * Rimuova la tag passata come parametro 
     * dall'elenco di tag associate alla task corrente
     * passata come parametro
     */
    @Transactional
    public Task removeTagFromTask(Task task, Tag tag) {
    	List<Tag> tagList = task.getTags();
    	tagList.remove(tag);
    	task.setTags(tagList);
    	return this.taskRepository.save(task);
    }

}
