package it.uniroma3.siw.progetto.repository;


import org.springframework.data.repository.CrudRepository;
import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;

import java.util.Optional;

/**
 * Interfaccia per le operazioni Crud sulle credenziali
 */
public interface CredentialsRepository extends CrudRepository<Credentials, Long> {

    /**
     * Recupera le credenziali passando un username
     */
    public Optional<Credentials> findByUserName(String username);
   
}



