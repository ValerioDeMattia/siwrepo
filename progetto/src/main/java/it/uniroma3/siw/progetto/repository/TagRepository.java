package it.uniroma3.siw.progetto.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Tag;

/**
 * Interfaccia CRUD per operazioni di repository sulla classe Tag
 */
public interface TagRepository extends CrudRepository<Tag, Long>{
	
}
