package it.uniroma3.siw.progetto.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.model.Task;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.ProjectRepository;
import it.uniroma3.siw.progetto.repository.TaskRepository;

/**
 * Gestisce la logica per il modello Project.
 * Si parla principalmente di uso di operazioni CRUD e condivisione dei progetti con altri utenti.
 */
@Service
public class ProjectService {

	@Autowired
	protected ProjectRepository projectRepository;

	@Autowired
	protected TaskRepository taskRepository;


	/**
	 * Recupera tutti i progetti salvati sul DB
	 */
	@Transactional
	public List<Project> getAllProjects() {
		List<Project> result = new ArrayList<>();
        Iterable<Project> iterable = this.projectRepository.findAll();
        for(Project p : iterable)
            result.add(p);
        return result;
	        
	}
	
	
	
	/**
	 * Recupera un progetto dal DB in base al suo ID.
	 * @param id, l'ID usato per recuperare il progetto dal DB
	 * @return il progetto il cui ID corrisponde con il valore passato
	 */
	@Transactional
	public Project getProject(long id) {
		Optional<Project> result = this.projectRepository.findById(id);
		return result.orElse(null);
	}
	
	


	/**
	 * Effettua la save sul DB del progetto passato
	 * @param project, progetto da salvare sul DB
	 * @return il progetto viene salvato
	 */
	@Transactional
	public Project saveProject(Project project) {
		return this.projectRepository.save(project);
	}

	/**
	 * Effettua la cancellazione del progetto dal DB
	 * @param project il progetto da eliminare dal DB
	 */
	@Transactional
	public void deleteProject(Project project) {
		this.projectRepository.delete(project);
	}


	/**
	 * Condivide il progetto passato con l'utente passato ed effettua la save sul DB
	 * @param project, il progetto da conndividere con l'utente
	 * @param user, l'utente con cui condividere il progetto
	 * @return il progetto condiviso
	 */
	@Transactional
	public Project shareProjectWithUser(Project project, User user) {
		project.addMember(user);
		return this.projectRepository.save(project);
	}

	/**
	 * Condivide il progetto passato con l'utente passato ed effettua la save sul DB
	 * @param project, il progetto da conndividere con l'utente
	 * @param user, l'utente con cui condividere il progetto
	 * @return il progetto condiviso
	 */
	@Transactional
	public Project removeUserFromProject(Project project, User user) {
		List<User> listOfMembers = project.getMembers();
		listOfMembers.remove(user);
		List<Task> listOfTasks = project.getTasks();
		for(Task t : listOfTasks) {
			if(t.getAssignedTo()!=null && t.getAssignedTo().equals(user)) {
				t.setAssignedTo(null);
				taskRepository.save(t);
			}
		}
		project.setMembers(listOfMembers);
		return this.projectRepository.save(project);
	}

	/**
	 * Aggiunge un nuovo task al progetto e lo salva nel db
	 * @param project, il progetto 
	 * @param task, il task da aggiungere al progetto
	 * @return il progetto con il nuovo task
	 */
	@Transactional
	public Project addTaskToProject(Project project, Task task) {
		project.addTask(task);
		return this.projectRepository.save(project);
	}



	/**
	 * Elimina una task dal db e la rimuove dal progetto in cui compare
	 * @param project task, la task da eliminare e il progetto in cui compare
	 * @param project, il progetto da cui rimuovere l'attività
	 * @return il progetto condiviso
	 */
	@Transactional
	public Project removeTaskFromProject(Project project, Task task) {
		List<Task> listOfTasks = project.getTasks();
		listOfTasks.remove(task);
		project.setTasks(listOfTasks);
		return this.projectRepository.save(project);
	}



	/**
	 * Aggiunge un nuovo tag al progetto e lo salva nel db
	 * @param project, il progetto 
	 * @param tag, il tag da aggiungere al progetto
	 * @return il progetto con il nuovo tag
	 */
	@Transactional
	public Project addTagToProject(Project project, Tag tag) {
		project.addTag(tag);
		return this.projectRepository.save(project);
	}


	/**
	 * Elimina un tag dal db e lo rimuove dalle task in cui compare
	 * @param project tag, il tag da eliminare e il progetto in cui compare
	 * @param project, il progetto da cui rimuovere il tag
	 * @return il progetto condiviso
	 */
	@Transactional
	public Project removeTagFromProject(Project project, Tag tag) {
		List<Tag> listOfTags = project.getTags();
		listOfTags.remove(tag);
		List<Task> listOfTasks = project.getTasks();
		for(Task t : listOfTasks) {
			List<Tag> tags = t.getTags();
			tags.remove(tag);
			t.setTags(tags);
			this.taskRepository.save(t);
		}
		project.setTags(listOfTags);
		return  this.projectRepository.save(project);
	}

}
