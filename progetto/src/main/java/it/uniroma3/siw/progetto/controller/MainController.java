package it.uniroma3.siw.progetto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.service.CredentialsService;
import it.uniroma3.siw.progetto.service.ProjectService;
/**
 * MainController per gestire le interazioni con la home page
 * che non si riferisce a specifiche entità.
 */
@Controller
public class MainController {

	@Autowired
	CredentialsService credetialsService;
	
	@Autowired
	ProjectService projectService;
	
    public MainController() {
    }

    /**
     * Metodo invocato sulla GET per "/" o "/index"
     * Prepara e gestisce la vista index.
     *
     * @param il modello della richiesta
     * @return il modello target "index"
     */
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }
	
	
	/**
	 * Metodo per la visione da parte dell'admin
	 * di tutti gli utenti salvati sul database
	 * 
	 * @param model ,il modello della richiesta
	 * @return il modello target "adminUsers"
	 */
	@RequestMapping(value = {"/admin/user"}, method= RequestMethod.GET)
	public String showAllUsers(Model model) {
		List<Credentials> allCreds = credetialsService.getAllCredentials();
		model.addAttribute("allCreds", allCreds);
		return "adminUsers";
	}
	

	/**
	 * Metodo per l'admin usato per eliminare
	 * un utente dal database
	 * @param userId, l'id dell'utente da eliminare
	 * @return il modello target verso la pagina di conferma "adminDeleteSuccessful"
	 */
	@RequestMapping(value = {"/admin/user/{id}/delete"}, method = RequestMethod.GET)
	public String deleteUser(@PathVariable(value = "id") String userId,
			Model model) {
		if(userId!=null) {
			Credentials userCreds = credetialsService.getCredentials(Long.parseLong(userId));
			credetialsService.deleteCredentials(userCreds);
			return "adminDeleteSuccessful";
		}
		return "adminUser";
	}
	
	
	/**
	 * Metodo per la visione da parte dell'admin
	 * di tutti i progetti salvati sul database
	 * 
	 * @param model ,il modello della richiesta
	 * @return il modello target "adminProjects"
	 */
	@RequestMapping(value = {"/admin/project"}, method= RequestMethod.GET)
	public String  showAllProjects(Model model) {
		List<Project> allProjects = projectService.getAllProjects();
		model.addAttribute("allProjects", allProjects);
		return "adminProjects";
	}
	


	/**
	 * Metodo per l'admin usato per eliminare
	 * un progetto dal database
	 * 
	 * @param projectId, l'id del progetto da eliminare
	 * @return il modello target verso la pagina di conferma "adminDeleteProjectSuccessful"
	 */
	@RequestMapping(value = {"/admin/project/{id}/delete"}, method = RequestMethod.GET)
	public String deleteProject(@PathVariable(value = "id") String projectId,
			Model model) {
		if(projectId!=null) {
			Project project = projectService.getProject(Long.parseLong(projectId));
			projectService.deleteProject(project);
			return "adminDeleteProjectSuccessful";
		}
		return "adminProjects";
	}
}