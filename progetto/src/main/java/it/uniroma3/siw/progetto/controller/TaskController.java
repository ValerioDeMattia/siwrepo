package it.uniroma3.siw.progetto.controller; 

import java.util.List; 

import javax.validation.Valid; 

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Controller; 
import org.springframework.ui.Model; 
import org.springframework.validation.BindingResult; 
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 

import it.uniroma3.siw.progetto.controller.session.SessionData; 
import it.uniroma3.siw.progetto.controller.validation.CommentValidator; 
import it.uniroma3.siw.progetto.controller.validation.CredentialsValidator; 
import it.uniroma3.siw.progetto.controller.validation.ProjectValidator; 
import it.uniroma3.siw.progetto.controller.validation.TagValidator; 
import it.uniroma3.siw.progetto.controller.validation.TaskValidator; 
import it.uniroma3.siw.progetto.controller.validation.UserValidator; 
import it.uniroma3.siw.progetto.model.Comment; 
import it.uniroma3.siw.progetto.model.Project; 
import it.uniroma3.siw.progetto.model.Tag; 
import it.uniroma3.siw.progetto.model.Task; 
import it.uniroma3.siw.progetto.model.User; 
import it.uniroma3.siw.progetto.repository.ProjectRepository; 
import it.uniroma3.siw.progetto.service.CommentService; 
import it.uniroma3.siw.progetto.service.ProjectService; 
import it.uniroma3.siw.progetto.service.TagService; 
import it.uniroma3.siw.progetto.service.TaskService; 
import it.uniroma3.siw.progetto.service.UserService; 


/** 
 * Gestisce tutte le interazioni con i progetti 
 */ 
@Controller 
public class TaskController { 


	@Autowired 
	ProjectRepository projectRepository; 

	@Autowired 
	ProjectValidator projectValidator; 

	@Autowired 
	CommentValidator commentValidator; 

	@Autowired 
	CredentialsValidator credentialsValidator; 

	@Autowired 
	UserValidator userValidator; 

	@Autowired 
	TaskValidator taskValidator; 

	@Autowired 
	TagValidator tagValidator; 

	@Autowired 
	UserService userService; 

	@Autowired 
	CommentService commentService; 

	@Autowired 
	TaskService taskService; 

	@Autowired 
	ProjectService projectService; 

	@Autowired 
	TagService tagService; 

	@Autowired 
	SessionData sessionData; 





	/** 
	 * Metodo chiamato sulla GET dell'URL "/projects/{projectId}/info/tasks/{taskId}/details". 
	 * Questo metodo prepara la vista per la visualizzazione delle informazioni
	 * riguardo un'attività di un progetto di cui si ha proprietà
	 * 
	 * @param projectId taskId, l'id della task da visualizzare e l'id del progetto che la comprende
	 * @return Il target della vista come stringa di "/taskInfo" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/tasks/{taskId}/details" }, method = RequestMethod.GET) 
	public String TaskInfo(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			Model model) { 
		if (projectId!= null || taskId!= null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			model.addAttribute("currentProject", project); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("tagList", task.getTags()); 
			model.addAttribute("commentsList", task.getComments()); 
			return "taskInfo"; 
		} 
		return "projectInfo"; 
	} 





	/** 
	 * Metodo chiamato sulla GET dell'URL "/projects/{projectId}/info/tasks/{taskId}/details". 
	 * Questo metodo prepara la vista per la visualizzazione delle informazioni
	 * riguardo un'attività di un progetto di cui si fa parte
	 * 
	 * @param projectId taskId, l'id della task da visualizzare e l'id del progetto che la comprende
	 * @return Il target della vista come stringa di "/sharedTaskInfo" 
	 */ 
	@RequestMapping(value = { "/shared/{id_project}/info/tasks/{id_task}/details" }, method = RequestMethod.GET) 
	public String sharedTaskInfo(@PathVariable(value ="id_project") String projectId, 
			@PathVariable(value ="id_task") String taskId, 
			Model model) { 
		if (projectId!= null || taskId!= null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			model.addAttribute("currentProject", project); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("tagList", task.getTags()); 
			model.addAttribute("commentsList", task.getComments()); 
			return "sharedTaskInfo"; 
		} 
		return "sharedInfo"; 
	} 




	/** 
	 * metodo che prepara la view per l'aggiunta di un tag 
	 * (fra i disponibili aka quelli del progetto) 
	 * al task di cui sono state aperte le info 
	 *  
	 * @param id_project id_task, il task corrente e il project di cui fa parte 
	 * @return la view con i tag disponibili 
	 */ 
	@RequestMapping(value = {"/projects/{id_project}/info/task/{id_task}/tag/add"}, method = RequestMethod.GET) 
	public String addTagToTask(@PathVariable (value = "id_project") String projectId, 
			@PathVariable (value = "id_task") String taskId, 
			Model model) { 
		if(projectId!=null || taskId!=null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			List<Tag> projectTags = project.getTags(); 
			model.addAttribute("currentProject", project); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("projectTags", projectTags); 
			return "addTagToTask"; 
		} 
		return "taskInfo"; 
	} 


	/** 
	 * dopo aver premuto il link vicino alla tag in addTagToTask.html, aggiuge la tag alla task corrente 
	 *  
	 * @param gli id del progetto e task corrente e della tag selezionata 
	 * @return aggiunge il tag al task e prepara una view di conferma che riporta su owned 
	 */ 
	@RequestMapping(value = {"/projects/{id_project}/info/task/{id_task}/tag/{id_tag}/add"}, method = RequestMethod.GET) 
	public String addTagToTask(@PathVariable (value = "id_project") String projectId, 
			@PathVariable (value = "id_task") String taskId, 
			@PathVariable (value = "id_tag") String tagId, 
			Model model) { 
		Project project = projectService.getProject(Long.parseLong(projectId)); 
		Task task = taskService.getTask(Long.parseLong(taskId)); 
		model.addAttribute("currentTask", task); 
		model.addAttribute("currentProject", project); 
		if(projectId!=null || taskId!=null || tagId!=null) { 
			Tag tag = tagService.getTag(Long.parseLong(tagId)); 
			taskService.addTagToTask(task, tag); 
			return "tagAddedToTaskSuccessful"; 
		} 
		return "addTagToTask"; 
	} 

	/** 
	 * dopo aver premuto il link vicino alla tag in addTagToTask.html, aggiuge la tag alla task corrente 
	 *  
	 * @param gli id del progetto e task corrente e della tag selezionata 
	 * @return aggiunge il tag al task e prepara una view di conferma che riporta su owned 
	 */ 
	@RequestMapping(value = {"/projects/{id_project}/info/task/{id_task}/tag/{id_tag}/remove"}, method = RequestMethod.GET) 
	public String removeTagFromTask(@PathVariable (value = "id_project") String projectId, 
			@PathVariable (value = "id_task") String taskId, 
			@PathVariable (value = "id_tag") String tagId, 
			Model model) { 
		Project project = projectService.getProject(Long.parseLong(projectId)); 
		Task task = taskService.getTask(Long.parseLong(taskId)); 
		model.addAttribute("currentTask", task); 
		model.addAttribute("currentProject", project); 
		if(projectId!=null || taskId!=null || tagId!=null) { 
			Tag tag = tagService.getTag(Long.parseLong(tagId)); 
			taskService.removeTagFromTask(task, tag); 
			return "tagRemovedFromTaskSuccessful"; 
		} 
		return "taskInfo"; 
	} 

	/** 
	 * Metodo per assegnare un task 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/assign" }, method = RequestMethod.GET) 
	public String assignTask(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			Model model) { 
		if (projectId!= null || taskId!= null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			List<User> projectMembers = project.getMembers(); 
			model.addAttribute("projectMemberList", projectMembers); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("currentProject", project); 
			return "assignTask"; 
		} 
		return "taskInfo"; 
	} 

	/** 
	 * Metodo per assegnare un task 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/assign/{userId}" }, method = RequestMethod.GET) 
	public String assignTaskFromList(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			@PathVariable(value ="userId") String userId, 
			Model model) { 
		if (taskId!= null && userId!=null) { 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			User user = userService.getUser(Long.parseLong(userId)); 
			taskService.assignTaskToMember(task, user); 
			return "taskAssignSuccessful"; 
		} 
		return "assignTask"; 
	} 

	/** 
	 * Metodo per assegnare un task 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/deassign" }, method = RequestMethod.GET) 
	public String deassignUserFromTask(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			Model model) { 
		if (projectId!= null && taskId!=null) { 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			taskService.deassignMemberFromTask(task); 
			return "taskDeassignSuccessful"; 
		} 
		return "assignTask"; 
	} 

	/** 
	 * Metodo per aggiungere un commento 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/comment/add" }, method = RequestMethod.GET) 
	public String commentTask(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			Model model) { 
		if (projectId!= null && taskId!=null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("currentProject", project); 
			model.addAttribute("commentForm", new Comment()); 
			return "commentTask"; 
		} 
		return "taskInfo"; 
	} 

	/** 
	 * Metodo per aggiungere un commento 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/comment/add" }, method = RequestMethod.POST) 
	public String commentTaskPost(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			@Valid @ModelAttribute("commentForm") Comment comment, 
			BindingResult commentBindingResult, 
			Model model) { 
		Project project = projectService.getProject(Long.parseLong(projectId)); 
		Task task = taskService.getTask(Long.parseLong(taskId)); 
		model.addAttribute("currentTask", task); 
		model.addAttribute("currentProject", project); 
		this.commentValidator.validate(comment, commentBindingResult); 

		if(!commentBindingResult.hasErrors()) { 
			User user = sessionData.getLoggedUser(); 
			comment.setPostedBy(user); 
			commentService.saveComment(comment); 
			task.addComment(comment); 
			taskService.saveTask(task); 
			return "commentTaskSuccesful"; 
		} 
		return "commentTask"; 
	} 

	/** 
	 * Metodo per aggiungere un commento 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/update" }, method = RequestMethod.GET) 
	public String taskUpdate(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			Model model) { 
		if (projectId!= null && taskId!=null) { 
			Project project = projectService.getProject(Long.parseLong(projectId)); 
			Task task = taskService.getTask(Long.parseLong(taskId)); 
			model.addAttribute("currentTask", task); 
			model.addAttribute("currentProject", project); 
			model.addAttribute("taskForm", new Task()); 
			return "taskUpdate"; 
		} 
		return "taskInfo"; 
	} 

	/** 
	 * Metodo per aggiungere un commento 
	 * @param model ,il modello della richiesta 
	 * @return il modello target "assignTask" 
	 */ 
	@RequestMapping(value = { "/projects/{projectId}/info/task/{taskId}/update" }, method = RequestMethod.POST) 
	public String updateTaskPost(@PathVariable(value ="projectId") String projectId, 
			@PathVariable(value ="taskId") String taskId, 
			@Valid @ModelAttribute("taskForm") Task task, 
			BindingResult taskBindingResult, 
			Model model) { 
		Project project = projectService.getProject(Long.parseLong(projectId)); 
		Task oldTask = taskService.getTask(Long.parseLong(taskId)); 
		model.addAttribute("currentTask", task); 
		model.addAttribute("currentProject", project); 
		this.taskValidator.validate(task, taskBindingResult); 

		if(!taskBindingResult.hasErrors()) { 
			task.setId(oldTask.getId()); 
			task.setCreationTimestamp(oldTask.getCreationTimestamp()); 
			task.setComments(oldTask.getComments()); 
			task.setAssignedTo(oldTask.getAssignedTo()); 
			taskService.saveTask(task); 
			return "taskUpdateSuccsessful"; 
		} 
		return "taskUpdate"; 
	} 


}