package it.uniroma3.siw.progetto.controller.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.service.TagService;

/**
 * Validator per Tag
 */
@Component
public class TagValidator implements Validator {

    final Integer MAX_NAME_LENGTH = 100;
    final Integer MIN_NAME_LENGTH = 2;
    final Integer MAX_COLOR_LENGTH = 10;
    final Integer MIN_COLOR_LENGTH = 2;

    @Autowired
    TagService tagService;

    @Override
    public void validate(Object o, Errors errors) {
        Tag tag = (Tag) o;
        String name = tag.getName().trim();
        String color = tag.getColor().trim();

        if (name.isEmpty())
            errors.rejectValue("name", "required");
        else if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
            errors.rejectValue("name", "size");

        if (color.isEmpty())
            errors.rejectValue("color", "required");
        else if (color.length() < MIN_COLOR_LENGTH || color.length() > MAX_COLOR_LENGTH)
            errors.rejectValue("color", "size");
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Tag.class.equals(clazz);
    }

}
