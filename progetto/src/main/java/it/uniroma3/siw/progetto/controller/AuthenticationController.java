package it.uniroma3.siw.progetto.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;
import it.uniroma3.siw.progetto.controller.session.SessionData;
import it.uniroma3.siw.progetto.controller.validation.CredentialsValidator;
import it.uniroma3.siw.progetto.controller.validation.UserValidator;
import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.service.CredentialsService;

/**
 * Controller per gestire la registrazione e l'autenticazione.
 */
@Controller
public class AuthenticationController {

    @Autowired
    CredentialsService credentialsService;

    @Autowired
    UserValidator userValidator;

    @Autowired
    CredentialsValidator credentialsValidator;

    @Autowired
    SessionData sessionData;

    /**
     * Metodo chiamato sulla GET dell'URL "/register".
     * Questo metodo prepara la vista per la registrazione
     *
     * @param Il modello
     * @return Il target della vista come stringa di "/register"
     */
    @RequestMapping(value = { "/users/register" }, method = RequestMethod.GET)
    public String showRegisterForm(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("credentialsForm", new Credentials());

        return "registerUser";
    }

    /**
     * Metodo chiamato sulla POST dell'URL "/register".
     *
     * @param Gli oggetti derivati dalle form, User e Credentials
     * @return Il target della vista come stringa di "/register"
     */
    @RequestMapping(value = { "/users/register" }, method = RequestMethod.POST)
    public String registerUser(@Valid @ModelAttribute("userForm") User user,
                               BindingResult userBindingResult,
                               @Valid @ModelAttribute("credentialsForm") Credentials credentials,
                               BindingResult credentialsBindingResult,
                               Model model) {

        // validate user and credentials fields
        this.userValidator.validate(user, userBindingResult);
        this.credentialsValidator.validate(credentials, credentialsBindingResult);

        // if neither of them had invalid contents, store the User and the Credentials into the DB
        if(!userBindingResult.hasErrors() && ! credentialsBindingResult.hasErrors()) {
            // set the user and store the credentials;
            // this also stores the User, thanks to Cascade.ALL policy
            credentials.setUser(user);
            credentialsService.saveCredentials(credentials);
            return "registrationSuccessful";
        }
        return "registerUser";
    }
}