package it.uniroma3.siw.progetto.controller.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;

/**
 * SessionData è una classe usata per gesitre la sessione
 * La usiamo per gestire lo username e la password 
 * dell'utente corrente
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionData {

    /**
     * Utente corrente
     */
    private User user;

    /**
     * Credenziali utente
     */
    private Credentials credentials;

    @Autowired
    private CredentialsRepository credentialsRepository;

    /**
     * Prende dalla sessione le credenziali dell'utente loggato.
     * Se non sono salvati nella sessione, li recupera dal db e li salva
     * nella sessione.
     *
     * @return Le credenziali dell'utente
     */
    public Credentials getLoggedCredentials() {
        if (this.credentials == null)
            this.update();
        return this.credentials;
    }

    /**
     * Recupera dalla sessione l'utente loggato.
     * Se non è presente lo recupera dal db.
     *
     * @return L'utente loggato
     */
    public User getLoggedUser() {
        if (this.user == null)
            this.update();
        return this.user;
    }

    /**
     * Salva le credenziali e l'utente corrente
     */
    private void update() {
        UserDetails loggedUserDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        this.credentials = this.credentialsRepository.findByUserName(loggedUserDetails.getUsername()).get(); // can never be absent
        this.credentials.setPassword("[PROTECTED]");
        this.user = this.credentials.getUser();
    }
}