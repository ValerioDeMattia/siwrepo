package it.uniroma3.siw.progetto.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

@Entity
public class Project {

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO) 
	private Long id; 
	
	/**
	 * nome del progetto
	 */
	@Column(length = 100, nullable = false)
	private String name;
	
	/**
	 * data e ora di creazioe del progetto
	 */
	@Column(length = 100, nullable = false)
	private LocalDateTime creationDate;
	
	/**
	 * propietario del sistema, puo condividere il progetto con altri utenti
	 * */
    @ManyToOne(fetch = FetchType.EAGER)
    private User owner;
	
	
	/**
	 * utenti con cui il progetto è condiviso
	 * */
    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> members;
    
	/**
	 * lista di attività che fanno parte del progetto
	 * */
	@OneToMany(cascade= CascadeType.REMOVE, fetch= FetchType.EAGER)
	@JoinColumn(name="project_id")
	private List<Task> tasks;
	
	/**
	 * lista di tag che il prgetto offre per le proprie attività
	 */
	@OneToMany(cascade= CascadeType.REMOVE)
	@JoinColumn(name="project_id")
	private List<Tag> tags;

	public Project() {
		this.members= new ArrayList<>();
		this.tasks= new ArrayList<>();
		this.tags= new ArrayList<>();
	}
	
	public Project(String nome) {
		this.name = nome;
		this.members= new ArrayList<>();
		this.tasks= new ArrayList<>();
		this.tags= new ArrayList<>();
	}
	
	  /**
     * Questo metodo inizializza la creazione e l'ultimo update
     */
    @PrePersist
    protected void onPersist() {
        this.creationDate = LocalDateTime.now();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<User> getMembers() {
		return members;
	}

	public void setMembers(List<User> members) {
		this.members = members;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	public void addMember(User user) {
		if(!this.members.contains(user))
			this.members.add(user);
	}
	
	public void addTask(Task task) {
		if(!this.tasks.contains(task))
			this.tasks.add(task);
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public void addTag(Tag tag) {
		if(!this.tags.contains(tag))
			this.tags.add(tag);
	}
	
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(name, project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
    
    @Override
    public String toString() {

        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tasks=" + tasks +
                '}';
    }
}
