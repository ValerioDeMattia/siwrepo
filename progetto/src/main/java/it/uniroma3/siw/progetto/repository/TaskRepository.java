package it.uniroma3.siw.progetto.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Task;
/**
 * Interfaccia CRUD per operazioni di repository sulla classe Task
 */
public interface TaskRepository extends CrudRepository<Task, Long>{
    
}
