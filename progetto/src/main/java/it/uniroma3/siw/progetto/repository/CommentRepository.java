package it.uniroma3.siw.progetto.repository;

import org.springframework.data.repository.CrudRepository;
import it.uniroma3.siw.progetto.model.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {

}
