package it.uniroma3.siw.progetto.controller.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto.model.Comment;
import it.uniroma3.siw.progetto.service.CommentService;

/**
 * Validator per Comment
 */
@Component
public class CommentValidator implements Validator {

    final Integer MAX_NAME_LENGTH = 255;
    final Integer MIN_NAME_LENGTH = 5;

    @Autowired
    CommentService commentService;

    @Override
    public void validate(Object o, Errors errors) {
        Comment comment = (Comment) o;
        String description = comment.getDescription();

        if (description.isEmpty())
            errors.rejectValue("description", "required");
        else if (description.length() < MIN_NAME_LENGTH || description.length() > MAX_NAME_LENGTH)
            errors.rejectValue("description", "size");
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Comment.class.equals(clazz);
    }

}
