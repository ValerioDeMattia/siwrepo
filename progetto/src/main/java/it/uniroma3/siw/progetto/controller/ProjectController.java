package it.uniroma3.siw.progetto.controller;

import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto.controller.session.SessionData;
import it.uniroma3.siw.progetto.controller.validation.CredentialsValidator;
import it.uniroma3.siw.progetto.controller.validation.ProjectValidator;
import it.uniroma3.siw.progetto.controller.validation.TagValidator;
import it.uniroma3.siw.progetto.controller.validation.TaskValidator;
import it.uniroma3.siw.progetto.controller.validation.UserValidator;
import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.model.Task;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.ProjectRepository;
import it.uniroma3.siw.progetto.service.ProjectService;
import it.uniroma3.siw.progetto.service.TagService;
import it.uniroma3.siw.progetto.service.TaskService;
import it.uniroma3.siw.progetto.service.UserService;


/**
 * Gestisce tutte le interazioni con i progetti
 */
@Controller
public class ProjectController {


	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectValidator projectValidator;
	
	@Autowired
	CredentialsValidator credentialsValidator;
	
	@Autowired
	UserValidator userValidator;
	
	@Autowired
	TaskValidator taskValidator;
	
	@Autowired
	TagValidator tagValidator;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	TagService tagService;

	@Autowired
	SessionData sessionData;





	/**
	 * Metodo per la visione dei propri progetti 
	 * @param model ,il modello della richiesta
	 * @return il modello target "owned"
	 */
	@RequestMapping(value = { "/projects/owned" }, method = RequestMethod.GET)
	public String owned(Model model) {

		User loggedUser = sessionData.getLoggedUser();
		List<Project> ownedProjects = projectRepository.findByOwner(loggedUser);
		model.addAttribute("ownedProjects", ownedProjects);

		return "owned";
	}


	/**
	 * Metodo per la visione dei progetti di cui si fa parte
	 * @param model ,il modello della richiesta
	 * @return il modello target "shared"
	 */
	@RequestMapping(value = { "/projects/shared" }, method = RequestMethod.GET)
	public String shared(Model model) {

		User loggedUser = sessionData.getLoggedUser();
		List<Project> sharedProgects = projectRepository.findByMembers(loggedUser);
		model.addAttribute("sharedProjects", sharedProgects);

		return "shared";
	}
	
	
	
	
	
	/**
	 * Metodo per la visualizzazione delle informazioni
	 * riguardo il progetto selezionato
	 * (per progetti di cui si fa parte)
	 * 
	 * @param Il progetto da vsualizzare
	 * @return una view con le informazioni sul progetto
	 */
	@RequestMapping(value = {"/shared/projects/{id}/info"}, method = RequestMethod.GET)
	public String showSharedtInfo(@PathVariable(value ="id") String projectId,
			Model model) {
		if(projectId!= null) {
		Project currentProject = projectService.getProject(Long.parseLong(projectId) );
		model.addAttribute("currentProject", currentProject);
		model.addAttribute("projectOwner", currentProject.getOwner());
		model.addAttribute("projectMemberList", currentProject.getMembers());
		model.addAttribute("projectTaskList", currentProject.getTasks());
		model.addAttribute("projectTagList", currentProject.getTags());
		
		return "sharedInfo"; //view html in cui vengono mostrati i dettagli de progetto
		}
		return "shared";
	}
	

	
	/**
	 * Metodo chiamato sulla GET dell'URL "/register".
	 * Questo metodo prepara la vista per la registrazione di un nuovo progetto
	 *
	 * @param Il modello
	 * @return Il target della vista come stringa di "/createProject"
	 */
	@RequestMapping(value = { "/projects/new" }, method = RequestMethod.GET)
	public String showRegisterForm(Model model) {
		model.addAttribute("projectForm", new Project());
		return "newProject";
	}


	/**
	 * Metodo chiamato sulla POST dell'URL "/new".
	 *
	 * @param Gli oggetti derivati dalle form, Name
	 * @return Il target della vista come stringa di "/home"
	 */
	@RequestMapping(value = {"/projects/new" }, method = RequestMethod.POST)
	public String newRegister(@Valid @ModelAttribute("projectForm") Project project,
			BindingResult projectBindingResult, Model model) {
		
		//valida il nome del projetto inserito
		this.projectValidator.validate(project, projectBindingResult);
		
		//se non ci sono errori:
		if(!projectBindingResult.hasErrors()) {
			User owner = sessionData.getLoggedUser();
			project.setOwner(owner);
			projectService.saveProject(project);
			return "projectCreationSuccessful";
		}
		return "newProject";
	}
	
	
	/**
	 * Metodo per la visualizzazione delle informazioni
	 * riguardo il progetto selezionato
	 * 
	 * @param Il progetto da vsualizzare
	 * @return le informazioni sul progetto
	 */
	@RequestMapping(value = { "/projects/{projectId}/info" }, method = RequestMethod.GET)
	public String showProjectInfo(@PathVariable(value ="projectId") String id,
			Model model) {
		if(id!= null) {
		Project currentProject = projectService.getProject(Long.parseLong(id) );
		model.addAttribute("currentProject", currentProject);
		model.addAttribute("projectMemberList", currentProject.getMembers());
		model.addAttribute("projectTaskList", currentProject.getTasks());
		model.addAttribute("projectTagList", currentProject.getTags());
		
		return "projectInfo"; //view html in cui vengono mostrati i dettagli de progetto
		}
		return "owned";
	}
	
	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{projectId}/delete".
	 *
	 * @param id del progetto
	 * @return Il target della vista come stringa di "/projects/{projectId}/delete"
	 */
	@RequestMapping(value = {"/projects/{projectId}/delete" }, 
			method = RequestMethod.GET)
	public String deleteProject(@PathVariable(value="projectId") String id, 
            Model model) {
		if(id!=null) {
			Project project = projectService.getProject(Long.parseLong(id));
			projectService.deleteProject(project);
			return "projectDeleteSuccessful";
		}
		return "owned";
	}


	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{id_project}/info/tasks/add(id_project=${currentProject.id})".
	 * Questo metodo prepara la vista per la registrazione di una nuova attività
	 *
	 * @param Il modello
	 * @return Il target della vista come stringa di "/newTask"
	 */
	@RequestMapping(value = { "/projects/{projectId}/info/tasks/add" }, method = RequestMethod.GET)
	public String addNewTask(@PathVariable(value ="projectId") String id,
			Model model) {
		if (id!= null) {
			Project project = projectService.getProject(Long.parseLong(id));
			model.addAttribute("currentProject", project);
			model.addAttribute("taskForm", new Task());
			return "newTask";
		}
		return "projectInfo";
	}
	
	
	
	/**
	  * Metodo chiamato sulla POST dell'URL "/projects/{id_project}/info/tasks/add(id_project=${currentProject.id})".
	  *
	  * @param Gli oggetti derivati dalle form, Name e Description
	  * @return Il target della vista come stringa di "/home"
	  */
	 @RequestMapping(value = {"/projects/{projectId}/info/tasks/add" }, method = RequestMethod.POST)
	 public String addNewTask(@Valid @ModelAttribute("taskForm") Task task,
	   BindingResult taskBindingResult,
	   @PathVariable(value ="projectId") String id,
	   Model model) {
	  Project project = projectService.getProject(Long.parseLong(id));
	  model.addAttribute("currentProject", project);

	  //valida il nome della task inserita
	  this.taskValidator.validate(task, taskBindingResult);

	  //se non ci sono errori:
	  if(!taskBindingResult.hasErrors()) {
	   taskService.saveTask(task);
	   Project currentProject = projectService.getProject(Long.parseLong(id));
	   currentProject = projectService.addTaskToProject(currentProject, task);
	   return "addTaskSuccessful";
	  }
	  return "newTask";
	 }
	
	
	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{projectId}/info/members/add".
	 *
	 * @param id del progetto
	 * @return Il target della vista come stringa di "/projects/{projectId}/info/members/add"
	 */
	@RequestMapping(value = {"/projects/{projectId}/info/members/add" }, 
			method = RequestMethod.GET)
	public String addMemberView(@PathVariable(value="projectId") String id, 
            Model model) {
		if(id!=null) {
			Project project = projectService.getProject(Long.parseLong(id));
			model.addAttribute("currentProject", project);
			model.addAttribute("credentialsForm", new Credentials());
			return "addMemberToProject";
		}
		return "projectInfo";
	}
	
	/**
     * Metodo chiamato sulla POST dell'URL "/register".
     *
     * @param Gli oggetti derivati dalle form, User e Credentials
     * @return Il target della vista come stringa di "/register"
     */
    @RequestMapping(value = { "/projects/{projectId}/info/members/add" }, method = RequestMethod.POST)
    public String addMember(@Valid @ModelAttribute("credentialsForm") Credentials credentials,
    							BindingResult credentialsBindingResult,
                               @PathVariable(value="projectId") String id, 
                               Model model) {

        // validate user and credentials fields
        this.credentialsValidator.validateUserExistence(credentials, credentialsBindingResult);

        // if neither of them had invalid contents, store the User and the Credentials into the DB
        if(!credentialsBindingResult.hasErrors()) {
        	User user = userService.getUser(credentials.getUserName());
        	Project project = projectService.getProject(Long.parseLong(id));
            projectService.shareProjectWithUser(project, user);
            return "addMemberSuccessful";
        }
        return "addMemberToProject";
    }

    /**
	 * Metodo chiamato sulla GET dell'URL "/projects/{projectId}/delete".
	 *
	 * @param id del progetto
	 * @return Il target della vista come stringa di "/projects/{projectId}/delete"
	 */
	@RequestMapping(value = {"/projects/{projectId}/info/members/{memberId}/delete" }, 
			method = RequestMethod.GET)
	public String deleteMember(@PathVariable(value="projectId") String projectId, 
							   @PathVariable(value="memberId") String memberId, 
            Model model) {
		if(projectId!=null || memberId!=null) {
			Project project = projectService.getProject(Long.parseLong(projectId));
			User user = userService.getUser(Long.parseLong(memberId));
			projectService.removeUserFromProject(project, user);
			return "memberDeleteSuccessful";
		}
		return "owned";
	}
	
	
	
	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{id_project}/info/tasks/{id_task}/delete".
	 *
	 * @param id del progetto
	 * @return Il target della vista come stringa di "/projects/{id_project}/info/tasks/{id_task}/delete"
	 */
	@RequestMapping(value = {"/projects/{id_project}/info/tasks/{id_task}/delete" }, method = RequestMethod.GET)
	public String deleteTask(@PathVariable(value="id_project") String projectId, 
			@PathVariable(value="id_task") String taskId,
            Model model) {
		if(projectId!=null || taskId!=null) {
			Project project = projectService.getProject(Long.parseLong(projectId));
			Task task = taskService.getTask(Long.parseLong(taskId));
			projectService.removeTaskFromProject(project, task);
			return "taskDeleteSuccessful";
		}
		return "projectInfo";
	}

	
	
	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{id_project}/info/tags/{id_tag}/delete".
	 *
	 * @param id del progetto
	 * @return Il target della vista come stringa di "/projects/{id_project}/info/tags/{id_tag}/delete"
	 */
	@RequestMapping(value = {"/projects/{id_project}/info/tags/{id_tag}/delete" }, method = RequestMethod.GET)
	public String deleteTag(@PathVariable(value="id_project") String projectId, 
			@PathVariable(value="id_tag") String tagId,
            Model model) {
		if(projectId!=null || tagId!=null) {
			Project project = projectService.getProject(Long.parseLong(projectId));
			Tag tag = tagService.getTag(Long.parseLong(tagId));
			projectService.removeTagFromProject(project, tag);
			return "tagDeleteSuccessful";
		}
		return "projectInfo";
		
	}
	
	/**
	 * Metodo chiamato sulla GET dell'URL "/projects/{id_project}/info/tasks/add(id_project=${currentProject.id})".
	 * Questo metodo prepara la vista per la registrazione di una nuova attività
	 *
	 * @param Il modello
	 * @return Il target della vista come stringa di "/newTask"
	 */
	@RequestMapping(value = { "/projects/{projectId}/info/tags/add" }, method = RequestMethod.GET)
	public String addNewTag(@PathVariable(value ="projectId") String id,
			Model model) {
		if (id!= null) {
			Project project = projectService.getProject(Long.parseLong(id));
			model.addAttribute("currentProject", project);
			model.addAttribute("tagForm", new Tag());
			return "newTag";
		}
		return "projectInfo";
	}
	
	/**
	 * Metodo chiamato sulla POST dell'URL "/projects/{id_project}/info/tasks/add(id_project=${currentProject.id})".
	 *
	 * @param Gli oggetti derivati dalle form, Name e Description
	 * @return Il target della vista come stringa di "/home"
	 */
	@RequestMapping(value = {"/projects/{projectId}/info/tags/add" }, method = RequestMethod.POST)
	public String addNewTag(@Valid @ModelAttribute("tagForm") Tag tag,
			BindingResult tagBindingResult,
			@PathVariable(value ="projectId") String id,
			Model model) {

		Project project = projectService.getProject(Long.parseLong(id));
		model.addAttribute("currentProject", project);
		//valida il nome della task inserita
		this.tagValidator.validate(tag, tagBindingResult);

		//se non ci sono errori:
		if(!tagBindingResult.hasErrors()) {
			tagService.saveTag(tag);
			Project currentProject = projectService.getProject(Long.parseLong(id));
			currentProject = projectService.addTagToProject(currentProject, tag);
			return "addTagSuccessful";
		}
		return "newTag";
	}


	
	@RequestMapping(value = { "/projects/{projectId}/info/update" }, method = RequestMethod.GET)
	public String ProjectUpdate(@PathVariable(value ="projectId") String projectId,
			Model model) {
		if (projectId!= null) {
			Project project = projectService.getProject(Long.parseLong(projectId));
			model.addAttribute("currentProject", project);
			model.addAttribute("projectForm", new Project());
			return "projectUpdate";
		}
		return "projectInfo";
	}
	
	@RequestMapping(value = {"/projects/{projectId}/info/update" }, method = RequestMethod.POST)
	public String ProjectUpdatePost(@PathVariable(value ="projectId") String projectId,
			@Valid @ModelAttribute("projectForm") Project project,
			BindingResult projectBindingResult, Model model) {
		Project oldProject = projectService.getProject(Long.parseLong(projectId));
		model.addAttribute("currentProject", oldProject);
		this.projectValidator.validate(project, projectBindingResult);
		
		if(!projectBindingResult.hasErrors()) {
			project.setCreationDate(oldProject.getCreationDate());
			project.setId(oldProject.getId());
			project.setMembers(oldProject.getMembers());
			project.setOwner(oldProject.getOwner());
			project.setTags(oldProject.getTags());
			project.setTasks(oldProject.getTasks());
			projectService.saveProject(project);
			return "projectUpdateSuccessful";
		}
		return "projectInfo";
	}
	
}
