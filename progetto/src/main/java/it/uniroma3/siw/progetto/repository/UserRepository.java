package it.uniroma3.siw.progetto.repository;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.User;

import java.util.List;
/**
 * Interfaccia per le operazioni Crud utente
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
    /**
     * Recupera una lista di utenti che hanno la visibilità di un progetto
     * passando il progetto
     */
    public List<User> findByVisibleProjects(Project project);
    
}