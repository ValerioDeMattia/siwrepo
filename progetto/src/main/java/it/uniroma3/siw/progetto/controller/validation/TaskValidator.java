package it.uniroma3.siw.progetto.controller.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto.model.Task;
import it.uniroma3.siw.progetto.service.TaskService;

/**
 * Validatore per la classe Task
 */
@Component
public class TaskValidator implements Validator {

	final Integer MAX_NAME_LENGTH = 100;
	final Integer MIN_NAME_LENGTH = 2;

	@Autowired
	TaskService taskService;

	@Override
	public void validate(Object o, Errors errors) {
		Task task = (Task)o;
		String name = task.getName().trim();
		String desc = task.getDescription().trim();
		
		if(name.isEmpty())
    		errors.rejectValue("name","required");
    	else if(name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH)
    		errors.rejectValue("name", "size");
		
		if(desc.isEmpty())
    		errors.rejectValue("description","required");
    	else if(desc.length() < MIN_NAME_LENGTH || desc.length() > MAX_NAME_LENGTH)
    		errors.rejectValue("description", "size");
		
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return Task.class.equals(clazz);
	}

	
	
}

	