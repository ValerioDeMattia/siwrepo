package it.uniroma3.siw.progetto.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;

/**
 * Service che si occupa della logica delle credenziali.
 * Si occupa di recuperare e memorizzare le credenziali nel db.
 */
@Service
public class CredentialsService {

    @Autowired
    protected CredentialsRepository credentialsRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    /**
     * Questo metodo recupera le credenziali dal db 
     * passando l'id utente
     */
    @Transactional
    public Credentials getCredentials(long id) {
        Optional<Credentials> result = this.credentialsRepository.findById(id);
        return result.orElse(null);
    }

    /**
     * Questo metodo recupera le credenziali dal db 
     * passando un username
     */
    @Transactional
    public Credentials getCredentials(String username) {
        Optional<Credentials> result = this.credentialsRepository.findByUserName(username);
        return result.orElse(null);
    }

    /**
     * Questo metodo salva le credenziali nel db.
     * Prima di salvarle fa un encryption della password.
     * Passo un oggetto credenziali
     * ritorno le credenziali salvate
     * Lancia DataIntegrityViolationException se esistono già
     */
    @Transactional
    public Credentials saveCredentials(Credentials credentials) {
        credentials.setRole(Credentials.DEFAULT_ROLE);
        credentials.setPassword(this.passwordEncoder.encode(credentials.getPassword()));
        return this.credentialsRepository.save(credentials);
    }

    /**
     * Recupero una lista con tutte le credenziali nel db.
     */
    @Transactional
    public List<Credentials> getAllCredentials() {
        List<Credentials> result = new ArrayList<>();
        Iterable<Credentials> iterable = this.credentialsRepository.findAll();
        for(Credentials credentials : iterable)
            result.add(credentials);
        return result;
    }
    
    
    /**
     * Elimina le credenziali(e quindi l'utente associato) dal db
     */
    @Transactional
    public void deleteCredentials(Credentials credentials) {
    	this.credentialsRepository.delete(credentials);
    }
}
