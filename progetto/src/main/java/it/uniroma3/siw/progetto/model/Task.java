package it.uniroma3.siw.progetto.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

@Entity
public class Task {

	/**
     * Identificatore unico task
     */
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO) 
	private Long id;
	
	/**
     * nome del task
     */
	@Column(length = 100, nullable = false)
	private String name;
	
	/**
     * Descrizione del task
     */
	@Column(length = 100 ,nullable = false)
	private String description;
	
	/**
     * Data creazione del task
     */
	@Column(nullable = false)
	private LocalDateTime creationTimestamp;
	
	/**
     * Utente che ha visibilità del progetto al 
     * quale è stato assegnato un task
     */
	@OneToOne
	@JoinColumn(name="user_id")
	private User assignedTo;
	
	
	@OneToMany(cascade= CascadeType.REMOVE)                              //(fetch = FetchType.EAGER) NO, bug sulla lista dei task
	@JoinColumn(name="task_id")
	private List<Comment> comments;
	
	@OneToMany
	@JoinColumn(name="task_id")
	private List<Tag> tags;
	
	public Task() {
		this.comments = new ArrayList<>();
		this.tags = new ArrayList<>();
	}
	
	public Task(String name, String description) {
		this();
		this.name = name;
		this.description = description;
	}
	
	/**
     * Questo metodo inizializza la creazione
     */
    @PrePersist
    protected void onPersist() {
        this.creationTimestamp = LocalDateTime.now();
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(LocalDateTime creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	
	public User getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(User assignedTo) {
		this.assignedTo = assignedTo;
	}

	
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public void addComment(Comment comment) {
		this.comments.add(comment);
	}
	
	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public void addTag(Tag tag) {
		if(!this.tags.contains(tag))
			this.tags.add(tag);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationTimestamp == null) ? 0 : creationTimestamp.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (creationTimestamp == null) {
			if (other.creationTimestamp != null)
				return false;
		} else if (!creationTimestamp.equals(other.creationTimestamp))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", description=" + description + ", creationTimestamp="
				+ creationTimestamp + ", assignedTo=" + assignedTo + ", comments=" + comments + ", tags=" + tags + "]";
	}
}
