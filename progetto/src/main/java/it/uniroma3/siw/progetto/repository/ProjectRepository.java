package it.uniroma3.siw.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Project;
import it.uniroma3.siw.progetto.model.User;


/**
 * Interfaccia CRUD per operazioni di repository sulla classe Project
 */
public interface ProjectRepository extends CrudRepository<Project, Long>{
	
	/**
	 * Recupera tutti i progetti visibili all'utente passato
	 * @param member, l'utente il quale ha visibilità dei progetti richiesti
	 * @return la lista di projetti visibili all'utente passato
	 */
	public List<Project> findByMembers(User member);
	
	/**
	 * Recupera tutti i progetti di proprietà dell'utente passato
	 * @param owner, l'utente propietario dei progetti richiesti
	 * @return la lista di projetti appartententi all'utente passato
	 */
	public List<Project> findByOwner(User Owner);

}
