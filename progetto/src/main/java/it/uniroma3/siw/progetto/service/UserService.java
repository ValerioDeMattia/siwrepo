package it.uniroma3.siw.progetto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.CredentialsRepository;
import it.uniroma3.siw.progetto.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Servizio che si occupa della logica dell' utente.
 */
@Service
public class UserService {

    @Autowired
    protected UserRepository userRepository;
    
    @Autowired
    protected CredentialsRepository credentialsRepository;

    /**
     * Recupera l'utente passando un id
     */
    @Transactional
    public User getUser(long id) {
        Optional<User> result = this.userRepository.findById(id);
        return result.orElse(null);
    }

    /**
     * Salva l'utente passato nel db.
     * Ritorna l'utente salvato.
     * Lancia DataIntegrityViolationException se esiste un utente 
     * con lo stesso username dell'utente da salvare.
     */
    @Transactional
    public User saveUser(User user) {
        return this.userRepository.save(user);
    }

    /**
     * Questo metodo recupera una lista di tutti gli utenti nel db.
     */
    @Transactional
    public List<User> getAllUsers() {
        List<User> result = new ArrayList<>();
        Iterable<User> iterable = this.userRepository.findAll();
        for(User user : iterable)
            result.add(user);
        return result;
    }
    
    /**
     * Recupera l'utente passando un username
     */
    @Transactional
    public User getUser(String username) {
        Optional<Credentials> result = this.credentialsRepository.findByUserName(username);
        return (result.get().getUser());
    }
    
}
