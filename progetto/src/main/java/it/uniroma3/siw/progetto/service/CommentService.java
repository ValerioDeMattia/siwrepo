package it.uniroma3.siw.progetto.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Comment;
import it.uniroma3.siw.progetto.repository.CommentRepository;

/**
 * Service che si occupa della logica dei commenti.
 * Si occupa di recuperare e memorizzare i commenti nel db.
 */
@Service
public class CommentService {

    @Autowired
    protected CommentRepository commentRepository;
    
    /**
     * Recupera un commento passando un id
     */
    @Transactional
    public Comment getComment(long id) {
        Optional<Comment> result = this.commentRepository.findById(id);
        return result.orElse(null);
    }

    /**
     * Salva il commento passato nel db.
     * Ritorna il commento salvato.
     */
    @Transactional
    public Comment saveComment(Comment comment) {
        return this.commentRepository.save(comment);
    }

    /**
     * Questo metodo recupera una lista di tutti i commenti nel db.
     */
    @Transactional
    public List<Comment> getAllComments() {
        List<Comment> result = new ArrayList<>();
        Iterable<Comment> iterable = this.commentRepository.findAll();
        for(Comment user : iterable)
            result.add(user);
        return result;
    }
}
