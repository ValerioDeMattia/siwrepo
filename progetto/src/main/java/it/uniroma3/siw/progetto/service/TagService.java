package it.uniroma3.siw.progetto.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.progetto.model.Tag;
import it.uniroma3.siw.progetto.repository.TagRepository;

@Service
public class TagService {
	
	@Autowired
	protected TagRepository tagRepository;
	
	
	  /**
     * Recupera un tag dal DB in base al suo ID
     * @param id l'id dell'attività che vogliamo recuperare dal DB
     * @return il tag recuperato, oppure null se non c'è corrispondenza dell'ID nel DB
     */
	@Transactional
	public Tag getTag(long id) {
		Optional<Tag> result = this.tagRepository.findById(id);
        return result.orElse(null);
	}
	
	
    /**
     * Effettua la save del tag sul BD
     * @param tag ,il tag da salvare sul DB
     * @return il tag salvato sul DB
     */
    @Transactional
    public Tag saveTag(Tag tag) {
        return this.tagRepository.save(tag);
    }
    
    
    /**
     * Effettua la cancellazione di un tag dal DB
     * @param tag ,il tag eliminato dal db
     */
    @Transactional
    public void deleteTag(Tag tag) {
        this.tagRepository.delete(tag);
    }
    

}
