package it.uniroma3.siw.progetto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto.controller.session.SessionData;
import it.uniroma3.siw.progetto.controller.validation.CredentialsValidator;
import it.uniroma3.siw.progetto.controller.validation.UserValidator;
import it.uniroma3.siw.progetto.model.Credentials;
import it.uniroma3.siw.progetto.model.User;
import it.uniroma3.siw.progetto.repository.UserRepository;
import it.uniroma3.siw.progetto.service.CredentialsService;
import it.uniroma3.siw.progetto.service.UserService;

/**
 * Gestisce tutte le interazioni dello user
 */
@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserValidator userValidator;
    
    @Autowired
    CredentialsValidator credentialsValidator;

    @Autowired
    PasswordEncoder passwordEncoder;
    
    @Autowired
    CredentialsService credentialsService;

    @Autowired
    UserService userService;

    @Autowired
    SessionData sessionData;

    /**
     * Metodo invocato sulla GET per "/home"
     * Prepara e gestisce la home.
     *
     * @param Il modello della richiesta.
     * @return il modello target "home"
     */
    @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
    public String home(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("user", loggedUser);
        return "home";
    }

    /**
     * Metodo invocato sulla GET per "/users/me"
     * Prepara e gestisce la vista del proprio profilo.
     *
     * @param il modello della richiesta
     * @return il modello target "userProfile"
     */
    @RequestMapping(value = { "/users/me" }, method = RequestMethod.GET)
    public String me(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        Credentials credentials = sessionData.getLoggedCredentials();
        System.out.println(credentials.getPassword());
        model.addAttribute("user", loggedUser);
        model.addAttribute("credentials", credentials);

        return "userProfile";
    }
    
    /**
     * Metodo invocato sulla GET per "/users/me/update"
     * Prepara e gestisce la vista per fare update delle 
     * info profilo.
     *
     * @param il modello della richiesta
     * @return il modello target "update"
     */
    @RequestMapping(value = { "/users/me/update" }, method = RequestMethod.GET)
    public String showUpdateForm(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("credentialsForm", new Credentials());

        return "userProfileUpdate";
    }
    
    /**
     * Metodo chiamato sulla POST dell'URL "/users/me/update".
     *
     * @param Gli oggetti derivati dalle form, User e Credentials
     * @return Il target della vista di conferma come stringa di "/updateSuccsessful"
     */
    @RequestMapping(value = { "/users/me/update" }, method = RequestMethod.POST)
    public String updateUser(@Valid @ModelAttribute("userForm") User user,
                               BindingResult userBindingResult,
                               @Valid @ModelAttribute("credentialsForm") Credentials credentials,
                               BindingResult credentialsBindingResult,
                               Model model) {
        this.userValidator.validate(user, userBindingResult);
        this.credentialsValidator.validate(credentials, credentialsBindingResult);

        // if neither of them had invalid contents, store the User and the Credentials into the DB
        if(!userBindingResult.hasErrors() && ! credentialsBindingResult.hasErrors()) {
            
        	User oldLoggedUser = sessionData.getLoggedUser();
            Credentials oldCredentials = sessionData.getLoggedCredentials();
            
            credentials.setId(oldCredentials.getId());
            user.setId(oldLoggedUser.getId());
            credentials.setUser(user);
            
            credentialsService.saveCredentials(credentials);
            userService.saveUser(user);
            
            return "updateSuccsessful";
        }
        return "userProfileUpdate";
    }

    /**
     * Metodo invocato sulla GET per "/admin"
     * Prepara e gestisce la vista admin.
     *
     * @param il modello della richiesta
     * @return il modello target "admin"
     */
    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("user", loggedUser);
        return "admin";
    }

}
